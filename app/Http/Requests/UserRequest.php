<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|min:3|max:25',
            'age' => 'required|integer|min:18|max:100',
            'email' => 'required|email|unique:users|max:25',
            'password' => 'required|confirmed|min:8|max:50',
            'profile_picture' => 'required|mimes:jpg,jpeg,png|max:1024',
        ];

        if($this->method() == 'PATCH'){
            $rules['email'] = ['required|email',Rule::unique('users')->ignore($this->id)];
            $rules['profile_picture'] = 'mimes:jpg,jpeg,png|max:1024';
        }
        
        return $rules;
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }
}
