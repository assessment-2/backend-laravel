<?php

namespace App\Http\Requests;

use App\Models\Photo;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PhotoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'file_name' => 'required|unique:photos|max:25',
            'avatar' => 'required|mimes:jpeg,jpg,png|max:1024',
            'visibilty' => ['sometimes',Rule::in(array_values(Photo::VISIBILITIES))],
        ];

        if($this->method() == 'PATCH'){
            $rules = [
                'email' => 'required_if:visibility_type,private',
                'visibility_type' => ['required',Rule::in(array_values(Photo::VISIBILITIES))],
            ];  
        }
        
        return $rules;
    }

    public function response(array $errors)
    {
        return response()->json($errors, 422);
    }
}