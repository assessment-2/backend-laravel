<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Exception;

class UserController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        try{
            $data = $request->only('name','email','password','age');
            $file_name = upload_file($request->file('profile_picture'),'uploads/profile_pictures');
            $data['profile_picture'] = $file_name;
            $user = User::create($data);
            $token = $user->createToken('Laravel Password Grant Client')->accessToken;
            $response = ['token' => $token];
            return response($response, 200);
        }catch(Exception $e){
            return $e->getMessage();
        }
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        try{
            $user = User::find($id);
            $data = $request->only('name','email','password','age');
            if($request->hasFile('profile_picture')){
                $file_name = upload_file($request->file('profile_picture'),'uploads/profile_pictures',$user->profile_picture);
                $data['profile_picture'] = $file_name;
            }
            $user->update($data);
            return response()->json('success');
        }catch(Exception $e){
            return $e->getMessage();
        }
        
    }
}
