<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\PhotoRequest;
use App\Http\Resources\PhotoResource;
use App\Models\Photo;
use App\Models\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $photos = Photo::all();
        return response()->json(['data' => PhotoResource::collection($photos)]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhotoRequest $request)
    {
        try{
            $user = $request->user();
            $data = $request->only('file_name');
            $file_name = upload_file($request->file('avatar'),'uploads/user_photos/'.$user->id);
            $data['avatar'] = $file_name;
            $data['link'] = generateRandomString($user->id);
            $data['file_extension'] = $request->file('avatar')->getClientOriginalExtension();
            $data['visibility'] = Photo::VISIBILITIES['hidden'];
            $user->photos()->create($data);
            return response(['message' => 'suceess'], 200);
        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        try{
            $query = Photo::query();
            if($request->filled('date')){
                $date = Carbon::parse($request->date)->toDateString();
                $query->orWhereDate('created_at',$date);
            }

            if($request->filled('time')){
                $time = Carbon::parse($request->time)->toTimeString();
                $query->orWhereTime('created_at',$time);
            }

            if($request->filled('visibility')){
                $query->orWhere('visibility',$request->visibility);
            }

            if($request->filled('file_extension')){
                $query->orWhere('file_extension',$request->extension);
            }

            if($request->filled('file_name')){
                $query->orWhere('file_name','like',"%$request->file_name%");
            }
            $photos = $query->get();

            return response()->json(['data' => PhotoResource::collection($photos)]);
            
            return response()->json('success');
        }catch(Exception $e){
            return $e->getMessage();
        }
    }

    // update the visibilty of photo. if email is present then given email can access this photo.
    public function updateVisibility(PhotoRequest $request)
    {
        try{
            $photo = Photo::find($request->photo_id);
            if($request->filled('email')){
                $user_id = User::where('email',$request->email)->first()->id;
                if(!empty(request()->user()->privatePhotos()->first())){
                    $photo->privatePhotos()->attach($user_id);
                }
                $photo->visibility = $request->visibility;
            }
            if($request->filled('visibility')){
                $photo->visibility = $request->visibility;
            }
            return response()->json(['message' => 'Visibilty updated !']);
        }catch(Exception $e){
            return $e->getMessage();
        }
        
    }

    public function showPhoto($url){
        $photo = Photo::where('link',$url)->first();
        if(empty($photo)){
            return response()->json(['message' => 'image not found']);
        }
        if($photo->visibility == Photo::PUBLIC){
            return response()->json(['avatar' => 'uploads/user_photos/'.$photo->avatar]);
        }elseif($photo->visibility == Photo::PRIVATE && request()->user()->id != $photo->user_id ){
            $accessible = request()->user()->privatePhotos()->first();
            if(!empty($accessible)){
                return response()->json(['avatar' => 'uploads/user_photos/'.$photo->avatar]);
            }
        }elseif($photo->visibility == Photo::HIDDEN && request()->user()->id == $photo->user_id ){
            return response()->json(['avatar' => 'uploads/user_photos/'.$photo->avatar]);
        }else{
            return response()->json(['message' => 'image is not accessible']);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $photo = Photo::find($id);
            if($photo){
                $photo->delete();
            }
            return response()->json('success');
        }catch(Exception $e){
            return $e->getMessage();
        }
        
    }
}
