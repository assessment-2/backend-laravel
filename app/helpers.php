<?php

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
if(!function_exists('upload_file')){
    function upload_file($file, $path ,$old_file_name = null,){
        $path = public_path($path);
        $name = uniqid(rand(10,20)).time().'.'.$file->getClientOriginalExtension();
        if($old_file_name){
            unlink($path.'/'.$old_file_name);
        }
        $file->move($path,$name);
        return $name;
    }
}
if(!function_exists('generateRandomString')){
    function generateRandomString($user_id) {
        
        // String of all alphanumeric character
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        // Shuffle the $str_result and returns substring
        // of specified length
        return $user_id.substr(str_shuffle($str_result),0, 10).$user_id;
        }
}
