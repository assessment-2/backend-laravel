<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function privatePhotos()
    {
        return $this->belongsToMany(User::class,'private_photos');
    }

    const VISIBILITIES = [
        'private' => 'private',
        'public' => 'public',
        'hidden' => 'hidden',
    ];

    const PRIVATE = 'private';
    const PUBLIC = 'public';
    const HIDDEN = 'hidden';
}
