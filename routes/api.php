<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\PhotoController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function() {
    // return $request->user();
    Route::patch('user/update/{id}',[UserController::class, 'update']);
    Route::prefix('user/photos')->group(function(){
        Route::post('store',[PhotoController::class, 'store']);
        Route::get('index',[PhotoController::class, 'index']);
        Route::patch('update-visibility',[PhotoController::class, 'updateVisibility']);
        Route::post('search',[PhotoController::class, 'search']);
        Route::get('show/{url}',[PhotoController::class, 'showPhoto']);
        Route::delete('delete/{id}',[PhotoController::class, 'destroy']);
    });
});

Route::post('login',[AuthController::class, 'login']);
Route::post('user/store',[UserController::class, 'store']);